import com.sun.jna.platform.win32.User32;

import javax.swing.*;

public class Main {

    public static void main(String[] args) throws Exception {
        final JFrame frame = new JFrame();
        PopUpMessage.initUI(frame);
        Capture capture = new Capture();
        String justTranslated = "";
        String clipboardText = capture.getClipboardText();
        while (true) {
            try {
                Thread.sleep(200);
                String capturedText = capture.getSelectedText(User32.INSTANCE, Capture.CustomUser32.INSTANCE);
                if (!justTranslated.equals(capturedText) && !capturedText.equals(clipboardText)) {
                    String translatedText = Translator.translate("en", "vi", capturedText);
                    System.out.println(capturedText);
                    System.out.println("Translated text: " + translatedText);
                    PopUpMessage.setContent(translatedText, capturedText);
                    frame.repaint();
                    justTranslated = capturedText;
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
